# Python-DevOps-Aws

DevOps Training Material 

# Pre-Req

- [ ] Commitment to learn 

- [ ] Basic Computer skills (`https://en.wikiversity.org/wiki/Introduction_to_Computer_Science`)

- [ ] Win/Mac laptop or pc with usable operating condition(ofcourse with internet)

- [ ] Zoom Video Conference installed

# Contents 
- Linux 
- Python 
- AWS with Terraform and Python SDK
- DevOps 
- Project
- Optional DevOps Security

# Getting Started
- Create a new user in Gitlab.com (`https://docs.gitlab.com/ee/user/profile/account/create_accounts.html`)
- Install Git in your pc/laptop (`https://docs.gitlab.com/ee/topics/git/how_to_install_git/`)
- Create AWS Free Tier Account (`https://aws.amazon.com/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc`)
- Create a EC2 Instance in AWS for Linux prep (`https://www.guru99.com/creating-amazon-ec2-instance.html`) 

# Module#1-Linux
https://github.com/chassing/linux-sysadmin-interview-questions
